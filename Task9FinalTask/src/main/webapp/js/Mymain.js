// The root URL for the RESTful services
var rootURL = "http://localhost:8080/rest/notes";
var downloadURL = "http://localhost:8080/download";

var currentNote;

// Retrieve wine list when application starts 
getAll();


function add_file(noteId, obj) {
    console.log("send file " + new FormData(obj));
    set_path(noteId);
    $.ajax({
        type: "POST",
        data: new FormData(obj),
        url: downloadURL + "?id=" + noteId,
        contentType: false,
        cache: false,
        processData: false
    });
}

function set_path(noteId) {
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: "POST",
        url: rootURL + "/set_path",
        data: setNoteId(noteId),
        success: console.log('Set path')
    });
}

function deleting(noteId) {
    console.log(noteId);
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: "POST",
        url: rootURL + "/delete_note",
        data: setNoteId(noteId),
        success: console.log('Delete step 1')
    });
    console.log('go to getAll');
    getAll();
}

function clearing(noteId) {
    console.log(noteId);
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: "POST",
        url: rootURL + "/clear_note",
        data: setNoteId(noteId),
        success: console.log('Clear note ' + noteId)
    });
    console.log('go to getAll');
    getAll();
}

function check_done(noteId) {
    console.log(noteId);
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: "POST",
        url: rootURL + "/check_done",
        data: setNoteId(noteId),
        success: console.log('Checked step 1')
    });
    console.log('go to getAll');
    getAll();
}

function back_note(noteId) {
    console.log("BACk " + noteId);
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: "POST",
        url: rootURL + "/back_note",
        data: setNoteId(noteId),
        success: console.log('Back step 1')
    });
    console.log('go to getAll');
    getAll();
}

function setNoteId(noteId) {
    console.log(noteId + ' second');
    return JSON.stringify({
        "id": noteId
    });
}

function getAll() {
    console.log('getAll');
    $.ajax({
        type: 'GET',
        url: rootURL,
        dataType: "json", // data type of response
        success: renderList
    });
    console.log('AllGeted');
}

function set_note() {
    console.log('setNote');
    $.ajax({
        type: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },

        url: rootURL,
        dataType: "json",
        data: formToJSON(),
        success: console.log('Added')
    });
    renderList();
}

function renderList(data) {
    // JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
    var list = data == null ? [] : (data instanceof Array ? data : [data]);

    $('#TodayList li').remove();
    $('#TomorrowList li').remove();
    $('#SomedayList li').remove();
    $('#Buscet li').remove();
    $.each(list, function (index, note) {
        if (note.isDel == 0)  {
            console.log("note Don " + note.isDone);
            if (note.isDone == 0){
                console.log("note del" + note.type);
                if (note.type == 1) writeListToday(note);
                if (note.type == 2) writeListTomorrow(note);
                if (note.type == 3) writeListSomeday(note);
            }
        }
        note.isDel == 1 ? writeListBuscet(note) : false;
    });
}


function writeListSomeday(note) {
    ($('#SomedayList').append('<li>'
        + note.note +
        '<br> <button onclick="check_done(' + note.id + ')"> Done </button>' +
        '<br> <button onclick="deleting(' + note.id + ')"> Delete </button>' +
        '<br>' +
        '<form onsubmit="add_file(' + note.id + ', this)" >' +
        '<br><input type="file" id="file" name="file' + note.id + '"> <br>' +
        '<button type="submit">Submit</button><br>' +
        '</form>' +
        '<br>'
        + note.date + '</li>'))
}


function writeListToday(note) {
    ($('#TodayList').append('<li><a href="#" data-identity="' + note.id + '">'
        + note.note +
        '<br> <button onclick="check_done(' + note.id + ')"> Done </button>' +
        '<br> <button onclick="deleting(' + note.id + ')"> Delete </button>' +
        '<br>' +
        '<form onsubmit="add_file(' + note.id + ', this)" >' +
        '<br><input type="file" id="file" name="file' + note.id + '"> <br>' +
        '<button type="submit">Submit</button><br>' +
        '</form>' +
        '<br>'
        + note.date + '</a></li>'))
}

function writeListTomorrow(note) {
    ($('#TomorrowList').append('<li><a href="#" data-identity="' + note.id + '">'
        + note.note +
        '<br> <button onclick="check_done(' + note.id + ')"> Done </button>' +
        '<br> <button onclick="deleting(' + note.id + ')"> Delete </button>' +
        '<br>' +
        '<form onsubmit="add_file(' + note.id + ', this)" >' +
        '<br><input type="file" id="file" name="file' + note.id + '"> <br>' +
        '<button type="submit">Submit</button><br>' +
        '</form>' +
        '<br>'
        + note.date + '</a></li>'))
}


function writeListBuscet(note) {
    ($('#Buscet').append('<li><a href="#" data-identity="' + note.id + '">'
        + note.note +
        '<br> <button onclick="clearing(' + note.id + ')"> Delete </button>' +
        '<br> <button onclick="back_note(' + note.id + ')"> Back UP </button> </p>' +
        '</form>' +
        '<br>'
        + note.date + '</a></li>'))
}

// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
    var noteId = $('#noteId').val();
    return JSON.stringify({
        "id": noteId == "" ? null : noteId,
        "type": $('#type').val(),
        "note": $('#note').val(),
        "isDone": $('#isDone').val(),
        "isDel": $('#isDel').val(),
        "file": $('#file').val(),
        "date": $('#date').val()
    });
}


function renderDetails(note) {
    $('#note').val(note.note);
    $('#type').val(note.type);
    $('#isDone').val(note.isdone);
    $('#isVisible').val(note.isvisible);
    $('#path').val(note.path_to_file);
    $('#date').val(note.date);
}

