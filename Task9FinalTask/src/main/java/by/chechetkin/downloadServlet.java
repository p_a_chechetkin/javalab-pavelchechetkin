package by.chechetkin;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by Agentbars on 29.01.2018.
 */

public class downloadServlet extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static boolean isUnix() {

        String os = System.getProperty("os.name").toLowerCase();
        return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0);

    }

    public static boolean isWindows() {

        String os = System.getProperty("os.name").toLowerCase();
        //windows
        return (os.indexOf("win") >= 0);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        System.out.println(request.getContentType());
//        System.out.println(request.getContentLength());
        System.out.println("Download POST");
        String path = null;
        if (isUnix()) {
            System.out.println("OS Unix");
            path = "/media/agentbars/01CC3C7514C24D20/EPAM/RestJerseySer/src/main/resources";
        }
        if (isWindows()) {
            System.out.println("OS Windows");
            path = "C:\\temp";
        }
        final String filePath = path;

        System.out.println(filePath);
        BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
         

        FileWriter writer = new FileWriter(path + File.separator + "notes"+ request.getParameter("id")+".txt", false);
        
        System.out.println("filename " + request.getParameter("id"));
        String line;
		while ((line = reader.readLine()) != null) {
        	
            System.out.println(line);
            writer.write(line);
            writer.write("\n");
            writer.flush();
        }
        writer.close();
        
//        request.getRequestDispatcher("download.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Download GET");
        request.getRequestDispatcher("/download.jsp").forward(request, response);
    }
}
