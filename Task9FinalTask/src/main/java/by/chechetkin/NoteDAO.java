package by.chechetkin;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class NoteDAO {

    private static final String url = "jdbc:mysql://localhost:3306/notes";
    private static final String user = "root";
    private static final String password = "Agentbars0007";

    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;
    private static PreparedStatement ps;


    public List<Note> getAll() {
        List<Note> list = new ArrayList();
        String sql = "SELECT * FROM note ORDER BY id";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException se) { /*can't do anything */ }
            try {
                stmt.close();
            } catch (SQLException se) { /*can't do anything */ }
            try {
                rs.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
        return list;
    }

    public Note setNote(Note note){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            ps = con.prepareStatement("INSERT INTO note " +
                            "(type , note, isDone, isDel, path_to_file, date) " +
                            "VALUES (?, ?, ?, ?, ?, ?)", new String[] { "ID" });
            ps.setInt(1, note.getType());
            ps.setString(2, note.getNote());
            ps.setInt(3, note.getIsDone());
            ps.setInt(4, note.getIsDel());
            ps.setString(5, note.getFile());
            ps.setDate(6, null);
//            ps.setDate(6, (Date) note.getDate());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            int id = rs.getInt(1);
            note.setId(id);
            System.out.println(id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException se) { /*can't do anything */ }
//            try {
//                stmt.close();
//            } catch (SQLException se) { /*can't do anything */ }
            try {
                rs.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
        return note;
    }

    public void backNote(int noteId){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            ps = con.prepareStatement("UPDATE note " +
                    "SET type = '1' " +
                    "WHERE id = " + noteId + " ");
            ps.executeUpdate();
            ps = con.prepareStatement("UPDATE note " +
                    "SET isDel = '0' " +
                    "WHERE id = " + noteId + " ");
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException se) { /*can't do anything */ }
            try {
                stmt.close();
            } catch (SQLException se) { /*can't do anything */ }
            try {
                rs.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    
    public void setPath(int noteId){
    	try{
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection(url, user, password);
        ps = con.prepareStatement("UPDATE note " +
                        "SET path_to_file = 'notes"
                        + noteId + ".txt' " +
                        "WHERE id = " + noteId + " ");
        ps.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
        throw new RuntimeException(e);
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
    } finally {
        try {
            con.close();
        } catch (SQLException se) { /*can't do anything */ }
        try {
            stmt.close();
        } catch (SQLException se) { /*can't do anything */ }
        try {
            rs.close();
        } catch (SQLException se) { /*can't do anything */ }
    }
    }

    
    public void deleteNote(int noteId){
    	try{
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection(url, user, password);
        ps = con.prepareStatement("UPDATE note " +
                        "SET isDel = '1' " +
                        "WHERE id = " + noteId + " ");
        ps.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
        throw new RuntimeException(e);
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
    } finally {
        try {
            con.close();
        } catch (SQLException se) { /*can't do anything */ }
        try {
            stmt.close();
        } catch (SQLException se) { /*can't do anything */ }
        try {
            rs.close();
        } catch (SQLException se) { /*can't do anything */ }
    }
    }

    public void clearNote(int noteId){
    	try{
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection(url, user, password);
        ps = con.prepareStatement("DELETE  " +
                        "FROM note " +
                        "WHERE id = " + noteId + " ");
        ps.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
        throw new RuntimeException(e);
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
    } finally {
        try {
            con.close();
        } catch (SQLException se) { /*can't do anything */ }
        try {
            stmt.close();
        } catch (SQLException se) { /*can't do anything */ }
        try {
            rs.close();
        } catch (SQLException se) { /*can't do anything */ }
    }
    }

    protected Note processRow(ResultSet rs) throws SQLException {
        Note note = new Note();
        note.setId(rs.getInt("id"));
        note.setType(rs.getInt("type"));
        note.setNote(rs.getString("note"));
        note.setIsDone(rs.getInt("isDone"));
        note.setIsDel(rs.getInt("isDel"));
        note.setFile(rs.getString("path_to_file"));
        note.setDate(rs.getDate("date"));
        return note;
    }

	public void checkDone(int noteId) {
    	try{
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            ps = con.prepareStatement("UPDATE note " +
                            "SET isDone = '1' " +
                            "WHERE id = " + noteId + " ");
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException se) { /*can't do anything */ }
            try {
                stmt.close();
            } catch (SQLException se) { /*can't do anything */ }
            try {
                rs.close();
            } catch (SQLException se) { /*can't do anything */ }
        }

	}
}
