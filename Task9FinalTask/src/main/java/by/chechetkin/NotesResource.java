package by.chechetkin;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/notes")
public class NotesResource {
	
	NoteDAO dao = new NoteDAO();
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	public List<Note> getAll() {
		System.out.println("getAll");
		return dao.getAll();
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Note setNote(Note note){
		System.out.println("setNote");
		return dao.setNote(note);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("/delete_note")
	public void deleteNote(Note note){		
		System.out.println("deleteNote " + note.getId());
		dao.deleteNote(note.getId());
	}
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("/set_path")
	public void setPath(Note note){		
		System.out.println("set path " + note.getId());
		dao.setPath(note.getId());
	}
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("/back_note")
	public void backNote(Note note){		
		System.out.println("back note " + note.getId());
		dao.backNote(note.getId());
	}
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("/clear_note")
	public void clearNote(Note note){		
		System.out.println("clearNote " + note.getId());
		dao.clearNote(note.getId());
	}
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("/check_done")
	public void checkDone(Note note){		
		System.out.println("Note check done" + note.getId());
		dao.checkDone(note.getId());
	}

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/hi")
    public Response getHI() {
        return Response.ok("Hi").build();
    }
    
    

}