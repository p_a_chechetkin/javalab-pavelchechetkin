package by.chechetkin;

import java.util.Date;
public class Note{

    private int id = 0;
    private int type = 1;
    private String note = "startText";
    private int isDone;
    private int isDel;
    private String file;
    private Date date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsDone() {
        return isDone;
    }

    public void setIsDone(int done) {
        isDone = done;
    }

    public Note() {
    }

    public Note(String note,
                  String file,
                  int isDone,
                  int isDel,
                  Date date) {
        this.note = note;
        this.file = file;
        this.isDone = isDone;
        this.isDel = isDel;
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String value) {
        this.note = value;
    }
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getIsDel() {
        return isDel;
    }

    public void setIsDel(int deleted) {
        isDel = deleted;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
