--
-- ������ ������������ Devart dbForge Studio for MySQL, ������ 7.3.137.0
-- �������� �������� ��������: http://www.devart.com/ru/dbforge/mysql/studio
-- ���� �������: 25.02.2018 19:13:16
-- ������ �������: 5.7.21-log
-- ������ �������: 4.1
--


-- 
-- ���������� ������� ������
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- ���������� ����� SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--
-- ��������� ���� ������ �� ���������
--
USE notes;

--
-- ������� ������� "note"
--
DROP TABLE IF EXISTS note;

--
-- ��������� ���� ������ �� ���������
--
USE notes;

--
-- ������� ������� "note"
--
CREATE TABLE note (
  id int(11) NOT NULL AUTO_INCREMENT,
  note varchar(45) DEFAULT NULL,
  type int(11) UNSIGNED DEFAULT 1,
  isDone int(11) DEFAULT NULL,
  isDel int(11) DEFAULT NULL,
  path_to_file varchar(226) DEFAULT NULL,
  date datetime DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX id_UNIQUE (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 20
AVG_ROW_LENGTH = 1024
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

-- 
-- ����� ������ ��� ������� note
--
INSERT INTO note VALUES
(1, 'T1DN1DL1F+', 1, 0, 0, '', '2018-01-31 00:00:00'),
(3, 'do Someday', 3, 0, 0, 'notes3.txt', NULL),
(4, 'text', 1, 0, 0, NULL, NULL),
(5, 'text2', 2, 0, 0, NULL, NULL),
(6, 'text3', 3, 0, 0, NULL, NULL),
(7, 'text4', 1, 0, 0, NULL, NULL),
(8, 'text5', 2, 0, 1, NULL, NULL),
(9, 'text6', 3, 0, 0, NULL, NULL),
(10, 'text7', 1, 0, 0, NULL, NULL),
(11, 'text8', 2, 0, 0, NULL, NULL),
(12, NULL, 3, 0, 0, NULL, NULL),
(13, NULL, 1, 0, 0, NULL, NULL),
(15, '1', 1, 0, 0, '', NULL),
(16, '1', 1, 0, 0, '', NULL),
(17, 'WWW', 1, 0, 0, '', NULL),
(18, 'QWe', 3, 0, 0, '', NULL),
(19, 'qwerrrr', 3, 0, 0, '', NULL);
-- 
-- ������������ ���������� ����� SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- ��������� ������� ������
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
