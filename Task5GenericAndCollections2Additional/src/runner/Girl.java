package runner;

import box.BoxGirl;
import figures.FilmCircle;
import figures.PaperCircle;
import figures.PaperQuadrangle;
import interfaces.Paper;
import myComparater.FigureAreaComparator;
import myComparater.FigureColorComparator;
import myComparater.FigureFormComparator;
import myExcception.MyInterceptException;
import myExcception.ParameterException;
import myIOConnector.MyIOConnector;
import objectsLogic.Figure;

import java.io.IOException;
import java.util.Comparator;


public class Girl {
    public static void main(String[] args) throws IOException, ClassNotFoundException, ParameterException {

        BoxGirl boxGirl = getBoxGirl();

        // Создаем, кладем фигуры в коробку, после чего выводим содержимое коробки
        PaperQuadrangle paperQuadrangle = new PaperQuadrangle(1, 4);
        PaperQuadrangle paperQuadrangle1 = new PaperQuadrangle(2, 4, Paper.Color.BLUE);
        PaperCircle paperCircle = new PaperCircle(1, Paper.Color.GREEN);
        PaperCircle paperCircle1 = new PaperCircle(2, Paper.Color.BLUE);
        FilmCircle filmCircle = new FilmCircle(2);
        FilmCircle filmCircle2 = new FilmCircle(3);
        boxGirl.putFigureInBox(paperQuadrangle, paperCircle, filmCircle, filmCircle2, paperCircle1, paperQuadrangle1);
        boxGirl.printAllFigures();


        // компаратор формы сортируем и выводим на экран
        sortForm(boxGirl);


        // компаратор площади сортируем и выводим на экран
        sortArea(boxGirl);

        // компаратор цвета сортируем и выводим на экран
        sortColor(boxGirl);

        // Динамическое изменение коробки осуществляется за счет передачи фигур в List<>

        // Позволяет доставать из коробки фигуры как по одиночке, так и пачками
        actionGetFigures(boxGirl);

        // Позволяет складывать в коробку фигуры как по одиночке, так и пачками
        // одной фигур будет нехватать, потому что она уже есть в коробке
        actionPutFigure(boxGirl, paperQuadrangle, paperCircle, filmCircle);


        // получаем и выводим фигуры по параметрам
        actionPrintListsWithParam(boxGirl);


    }

    private static void actionPrintListsWithParam(BoxGirl boxGirl) {
        System.out.println("____Film____");
        for (Figure f : boxGirl.getFiguresFromFilm()) {
            System.out.println(f.getClass().getSimpleName());
        }
        System.out.println("____Paper____");
        for (Figure f : boxGirl.getFiguresFromPaper()) {
            System.out.println(f.getClass().getSimpleName());
        }
        System.out.println("__Circles__");
        for (Figure f : boxGirl.getCircles()) {
            System.out.println(f.getClass().getSimpleName());
        }
        System.out.println("___Quadrangles___");
        for (Figure f : boxGirl.getQuadrangles()) {
            System.out.println(f.getClass().getSimpleName());
        }
    }

    private static void actionPutFigure(BoxGirl boxGirl, PaperQuadrangle paperQuadrangle, PaperCircle paperCircle, FilmCircle filmCircle) {
        System.out.println("___Put Figures___");
        boxGirl.putFigureInBox(paperQuadrangle, paperCircle, filmCircle);
        boxGirl.printAllFigures();
    }

    private static void actionGetFigures(BoxGirl boxGirl) {
        System.out.println("___Get Figures___" +
                "\n Figures from box");
        for (Figure f : boxGirl.getFigure(1, 2, 3)) {
            System.out.println("f = " + f.getClass().getSimpleName());
        }
        System.out.println("Figures in box");
        boxGirl.printAllFigures();
    }

    private static void sortColor(BoxGirl boxGirl) {
        Comparator<Figure> figureComparator = new FigureColorComparator();
        boxGirl.getBoxForFigure().sort(figureComparator);
        System.out.println("_______COLOR_____");
        boxGirl.printAllFigures();
    }

    private static void sortArea(BoxGirl boxGirl) {
        Comparator<Figure> figureComparator = new FigureAreaComparator();
        boxGirl.getBoxForFigure().sort(figureComparator);
        System.out.println("_______AREA______");
        boxGirl.printAllFigures();
    }

    private static void sortForm(BoxGirl boxGirl) {
        Comparator<Figure> figureComparator = new FigureFormComparator();
        boxGirl.getBoxForFigure().sort(figureComparator);
        System.out.println("_____FORM________");
        boxGirl.printAllFigures();
    }

    private static BoxGirl getBoxGirl() {
        try {
            MyIOConnector.mySerializationForBox();
        } catch (MyInterceptException e) {
            e.printStackTrace();
        }

        BoxGirl boxGirl = null;
        try {
            boxGirl = MyIOConnector.myDeserialization();
        } catch (MyInterceptException e) {
            e.printStackTrace();
        }
        return boxGirl;
    }

}

