package interfaces;

public interface Film {

    default Color getColor() {
        return Color.COLORLESS;
    }

    enum Color {COLORLESS}

}
