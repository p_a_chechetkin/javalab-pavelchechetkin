package interfaces;

public interface Paper {

//    SetterForColor SETTER_FOR_COLOR = new SetterForColor();

    Color getColor();

    void setColor(Color color);

    enum Color {COLORLESS, RED, GREEN, BLUE}


    class SetterForColor {

        public static Color setColorForFigure(Color color, Color newColor) {
            if (color.equals(Color.COLORLESS)){
                return newColor;
            }
            return color;
        }

    }
}

