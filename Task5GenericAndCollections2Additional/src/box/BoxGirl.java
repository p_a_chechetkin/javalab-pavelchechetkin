package box;


import interfaces.Film;
import interfaces.Paper;
import objectsLogic.Circle;
import objectsLogic.Figure;
import objectsLogic.Quadrangle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class BoxGirl implements Serializable {

    private static final long serialVersionUID = -596143772957932271L;
    private List<Figure> boxForFigure = new ArrayList<>();

    public void printAllFigures() {
        for (Figure f : boxForFigure) {
            System.out.print(f.getClass().getSimpleName() + "  ");
            if (f instanceof Circle) {
                System.out.print(f.area() + " , ");
                if (f instanceof Paper) {
                    System.out.println(((Paper) f).getColor());
                } else {
                    System.out.println(((Film) f).getColor());
                }
            }
            if (f instanceof Quadrangle) {
                System.out.print(f.area() + " , ");
                if (f instanceof Paper) {
                    System.out.println(((Paper) f).getColor());
                }
            }
        }
    }

    public List<Figure> getBoxForFigure() {
        return boxForFigure;
    }

    public void putFigureInBox(Figure... figures) {
        for (Figure figure : figures) {
            if (!boxForFigure.contains(figure)) {
                boxForFigure.add(figure);
            }
        }
    }

    public Figure lookAtFigure(int number) {
        System.out.println("You look at  " + boxForFigure.get(--number));
        return boxForFigure.get(number);

    }

    public List<Figure> getFiguresFromFilm() {
        List<Figure> filmList = new ArrayList<>();
        for (Figure f : boxForFigure) {
            if (f instanceof Film) {
                filmList.add(f);
            }
        }
        return filmList;
    }

    public List<Figure> getFiguresFromPaper() {
        List<Figure> paperList = new ArrayList<>();
        for (Figure f : boxForFigure) {
            if (f instanceof Paper) {
                paperList.add(f);
            }
        }
        return paperList;
    }

    public List<Figure> getFigure(int... numbers) {
        List<Figure> listFiguresToReceiveet = new ArrayList<>();
        Arrays.sort(numbers);
        int count = 0;
        for (int number : numbers) {
            number -= count;
            listFiguresToReceiveet.add(boxForFigure.get(--number));
            boxForFigure.remove(number);
            count++;
        }
        return listFiguresToReceiveet;
    }

    public void replaceThisFigure(int number, Figure newFigure) {
        if (!boxForFigure.contains(newFigure)) {
            boxForFigure.set(--number, newFigure);
            System.out.println("Replaced figure");
        } else {
            System.out.println("The figure " + newFigure.getClass().getTypeName() + " already in box");
        }
    }

    public void viewAll() {
        double area = 0, perimetr = 0;
        for (Figure f : boxForFigure) {
            area += f.area();
            perimetr += f.perimeter();
        }
        System.out.println("Figures in box " + this + ": " + boxForFigure.size() + "\n Summary area = " + area
                + "\n Summary perimeter = " + perimetr + "\n");
    }

    public List<Figure> getQuadrangles() {
        List<Figure> quadrangleList = new ArrayList<>();
        for (Figure f : boxForFigure) {
            if (f instanceof Quadrangle) {
                quadrangleList.add(f);
            }
        }
        return quadrangleList;
    }

    public List<Figure> getCircles() {
        List<Figure> circleList = new ArrayList<>();
        for (Figure f : boxForFigure) {
            if (f instanceof Circle) {
                circleList.add(f);
            }
        }
        return circleList;
    }
}
