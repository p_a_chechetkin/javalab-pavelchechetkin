package myIOConnector;

import box.BoxGirl;
import myExcception.MyInterceptException;

import java.io.*;

public class MyIOConnector {

    private static final String FILE_PATH = "Task4Exceptions\\src\\serializedObjects\\BoxGirl.out";
    private static final String FILE_PATH_BEGIN = "Task4Exceptions\\src\\serializedObjects\\BoxGirl";
    private static final String FILE_PATH_END = ".out";

    public static BoxGirl myDeserialization() throws MyInterceptException {


        ObjectInputStream oin;
        BoxGirl boxGirl;
        try {
            oin = new ObjectInputStream(new FileInputStream(FILE_PATH));
            boxGirl = (BoxGirl) oin.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Catch Exception in Deserialization");
            throw new MyInterceptException(e);
        }
        return boxGirl;
    }

    public static BoxGirl myDeserializationForObject(Object object) throws MyInterceptException {

        String filePath = FILE_PATH_BEGIN + object.hashCode() + FILE_PATH_END + "1";
        ObjectInputStream oin;
        BoxGirl boxGirl = null;
        try {
            oin = new ObjectInputStream(new FileInputStream(filePath));
            if (object instanceof BoxGirl) {
                boxGirl = (BoxGirl) oin.readObject();
            }
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Catch Exception in DeserializationForObject");
            throw new MyInterceptException(e);
        }
        return boxGirl;
    }

    public static void mySerializationForBox() throws MyInterceptException {
        ObjectOutputStream oos;
        BoxGirl box = new BoxGirl();
        try {
            oos = new ObjectOutputStream(new FileOutputStream(FILE_PATH));
            oos.writeObject(box);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            System.out.println("Catch Exception in DeserializationForObject");
            throw new MyInterceptException(e);
        }
    }

    public static void mySerializationForObject(Object object) throws MyInterceptException {
        String filePath = FILE_PATH_BEGIN + object.hashCode() + FILE_PATH_END;
        ObjectOutputStream oos;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(filePath));
            oos.writeObject(object);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            System.out.println("Catch Exception in Serialization");
            throw new MyInterceptException(e);
        }
    }
}
