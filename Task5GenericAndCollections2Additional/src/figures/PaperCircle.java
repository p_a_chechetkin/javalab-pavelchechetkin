package figures;

import interfaces.Paper;
import myExcception.ParameterException;
import objectsLogic.Circle;

public class PaperCircle extends Circle implements Paper {


    private Color color;

    public PaperCircle(double radius) throws ParameterException {
        setRadius(radius);
    }

    public PaperCircle(double radius, Color color) throws ParameterException {
        setRadius(radius);
        setColor(color);
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color newColor) {
        color = SetterForColor.setColorForFigure(color, newColor);
    }
}
