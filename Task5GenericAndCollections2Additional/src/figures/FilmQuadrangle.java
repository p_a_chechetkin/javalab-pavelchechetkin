package figures;

import interfaces.Film;
import myExcception.ParameterException;
import objectsLogic.Quadrangle;

public class FilmQuadrangle extends Quadrangle implements Film {
    public FilmQuadrangle() {
    }

    public FilmQuadrangle(double newSideA, double newSideB) throws ParameterException {
        setSideA(newSideA);
        setSideB(newSideB);
    }
}
