package myExcception;

import interfaces.Paper;

public class ParameterException extends Exception {
    private double param;
    private Paper.Color color;

    public ParameterException(double param) {
        super("Параметр должен быть положительным, а не  '" + param + "'");
        this.param = param;
    }
    public ParameterException(Paper.Color color) {
        super("Нельзя перекрашивать фигуру");
        this.color = color;
    }

    public double getParam() {
        return param;
    }

    public Paper.Color getColor() {
        return color;
    }
}
