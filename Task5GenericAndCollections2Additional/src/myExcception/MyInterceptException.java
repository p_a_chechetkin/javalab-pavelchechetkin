package myExcception;

public class MyInterceptException extends Exception{
    private Exception exception;

    public MyInterceptException (Exception ex){
        super(ex.getMessage());
        exception = ex;
    }

    public Exception getException() {
        return exception;
    }
}
