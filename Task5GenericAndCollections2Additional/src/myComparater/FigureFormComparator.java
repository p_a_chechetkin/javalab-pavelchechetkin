package myComparater;

import objectsLogic.Circle;
import objectsLogic.Figure;

import java.util.Comparator;

public class FigureFormComparator implements Comparator<Figure> {
    @Override
    public int compare(Figure o1, Figure o2) {
        if (o1 instanceof Circle) {
            return -1;
        } else if (o2 instanceof Circle) {
            return +1;
        } else return 0;
    }
}
