package myComparater;

import objectsLogic.Figure;

import java.util.Comparator;

public class FigureAreaComparator implements Comparator<Figure> {
    @Override
    public int compare(Figure o1, Figure o2) {
        return (o1.area() - o2.area() < 0) ? -1 : +1;
    }
}
