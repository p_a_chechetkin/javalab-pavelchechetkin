package myComparater;

import interfaces.Paper;
import objectsLogic.Figure;

import java.util.Comparator;

public class FigureColorComparator implements Comparator<Figure> {
    @Override
    public int compare(Figure o1, Figure o2) {
        if (o1 instanceof Paper && o2 instanceof Paper) {
            return ((Paper) o1).getColor().ordinal() - ((Paper) o2).getColor().ordinal();
        } else {
            if (o1 instanceof Paper) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}
