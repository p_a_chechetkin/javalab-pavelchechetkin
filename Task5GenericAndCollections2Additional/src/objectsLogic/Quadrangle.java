package objectsLogic;

import myExcception.ParameterException;

public class Quadrangle extends Figure {
    private transient double sideA, sideB;

    protected Quadrangle() {
    }

    public Quadrangle(double sideA, double sideB) throws ParameterException {
        setSideA(sideA);
        setSideB(sideB);
    }

    public double getSideA() {
        return sideA;
    }

    protected void setSideA(double sideA) throws ParameterException {
        if (sideA <= 0) throw new ParameterException(sideA);
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    protected void setSideB(double sideB) throws ParameterException {
        if (sideB <= 0) throw new ParameterException(sideB);
        this.sideB = sideB;
    }

    @Override
    public double perimeter() {
        return 2 * sideA + 2 * sideB;
    }

    @Override
    public double area() {
        return sideA * sideB;
    }
}
