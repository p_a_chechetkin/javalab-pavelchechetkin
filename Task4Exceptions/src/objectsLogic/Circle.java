package objectsLogic;

import myExcception.ParameterException;

public class Circle extends Figure {
    private static transient double radius;

    public Circle() {
    }

    public Circle(double radius) throws ParameterException {
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    protected void setRadius(double radius) throws ParameterException {
        if (radius <= 0) throw new ParameterException();
        Circle.radius = radius;
    }

    @Override
    public double perimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double area() {
        return Math.PI * radius * radius;
    }

}

