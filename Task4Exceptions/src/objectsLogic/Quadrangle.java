package objectsLogic;

import myExcception.ParameterException;

public class Quadrangle extends Figure {
    private static transient double sideA, sideB;

    public Quadrangle() {
    }

    public Quadrangle(double sideA, double sideB) throws ParameterException {
        setSideA(sideA);
        setSideB(sideB);
    }

    public double getSideA() {
        return sideA;
    }

    protected void setSideA(double sideA) throws ParameterException {
        if (sideA <= 0) throw new ParameterException();
        Quadrangle.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    protected void setSideB(double sideB) throws ParameterException {
        if (sideB <= 0) throw new ParameterException();
        Quadrangle.sideB = sideB;
    }

    @Override
    public double perimeter() {
        return 2 * sideA + 2 * sideB;
    }

    @Override
    public double area() {
        return sideA * sideB;
    }
}
