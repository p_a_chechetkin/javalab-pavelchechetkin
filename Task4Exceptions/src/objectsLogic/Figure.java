package objectsLogic;

import java.io.Serializable;

public abstract class Figure implements Serializable {

    public abstract double perimeter();


    public abstract double area();
}
