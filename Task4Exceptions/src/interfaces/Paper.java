package interfaces;

import myExcception.ParameterException;
import objectsLogic.Figure;
import java.util.HashMap;

public interface Paper {

    ClassColor classColor = new ClassColor();

    default Color getColor() {
        return classColor.getColor((Figure) this);
    }

    default void setColor(Color color) throws ParameterException {
        classColor.setColorForFigure((Figure) this, color);
    }

    enum Color {RED, GREEN, BLUE, WHITE}


    class ClassColor {
        HashMap<Figure, Color> figuresWithColors = new HashMap<>();

        Color getColor(Figure figure) {
            return figuresWithColors.getOrDefault(figure, Color.WHITE);
        }

        void setColorForFigure(Figure figure, Color color) throws ParameterException {
            if (!(figuresWithColors.containsKey(figure))) {
                this.figuresWithColors.put(figure, color);
            } else throw new ParameterException();
        }

    }
}

