package runner;

import box.BoxGirl;
import figures.FilmCircle;
import figures.FilmQuadrangle;
import figures.PaperQuadrangle;
import myExcception.CatcherException;
import myExcception.ParameterException;
import myIOConnector.MyIOConnector;

import java.io.IOException;


public class Girl {
    public static void main(String[] args) throws IOException, ClassNotFoundException, ParameterException {

        // Сериализуем пустую коробку
        // Корректное выполнение
        try {
            MyIOConnector.mySerializationForBox();
        } catch (CatcherException e) {
            e.printStackTrace();
        }

        // Десериализуем и инициализируем объект коробки
        // Корректное выполнение
        BoxGirl boxGirl = null;
        try {
            boxGirl = MyIOConnector.myDeserialization();
        } catch (CatcherException e) {
            e.printStackTrace();
        }

        // Создаем, кладем фигуры в коробку, после чего выводим содержимое коробки
        PaperQuadrangle paperQuadrangle = new PaperQuadrangle(1, 4);
        FilmCircle filmCircle = new FilmCircle(2);
        FilmQuadrangle filmQuadrangle = new FilmQuadrangle(1, 4);
        boxGirl.putFigureInBox(paperQuadrangle, filmQuadrangle, filmCircle);

        System.out.println("Box1 with 3 figures");
        boxGirl.viewAll();


        // Сериализуем наполненную коробку в новый файл
        // Корректное выполнение
        try {
            MyIOConnector.mySerializationForObject(boxGirl);
        } catch (CatcherException e) {
            e.printStackTrace();
        }

        // Изменяем старую коробку и выводим ее на экран
        boxGirl.getFigure(1);
        System.out.println("Box1 after delete 1 figure");
        boxGirl.viewAll();

        // Десириализуем наполненную коробку
        // НЕкорректное выполнение, искомого файла нет
        BoxGirl newBoxGirl = null;
        try {
            newBoxGirl = MyIOConnector.myDeserializationForObject(boxGirl);
        } catch (CatcherException e) {
            e.printStackTrace();
        }

        // Выводим содержимое новой коробки
        // НЕкорректное выполнение, нет ссылки на коробку
        System.out.println("Saved Box2");
        try {
            newBoxGirl.viewAll();
        } catch (NullPointerException e){
            System.out.println("ERROR !!!  New BoxGirl is NULL");
        }
    }

}

