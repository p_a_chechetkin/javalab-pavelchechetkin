package myExcception;

public class CatcherException extends Exception{
    private Exception exception;

    public CatcherException(Exception ex){
        super(ex.getMessage());
        exception = ex;
    }

    public Exception getException() {
        return exception;
    }
}
