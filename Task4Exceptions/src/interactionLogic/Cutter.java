package interactionLogic;

// Cutting2

import figures.FilmCircle;
import figures.FilmQuadrangle;
import figures.PaperCircle;
import figures.PaperQuadrangle;
import interfaces.Film;
import interfaces.Paper;
import myExcception.ParameterException;
import objectsLogic.Circle;
import objectsLogic.Quadrangle;

public class Cutter {

    public static Quadrangle cutQuadrangle(Object originalFigure, double newSideA, double newSideB) throws ParameterException {
        if (originalFigure instanceof Paper) {
            if (originalFigure instanceof Circle) {
                PaperCircle originalCircle = (PaperCircle) originalFigure;
                if (CircleToQuadrangle(newSideA, newSideB, originalCircle.getRadius()))
                    return new PaperQuadrangle(newSideA, newSideB, ((PaperCircle) originalFigure).getColor());
            }
            if (originalFigure instanceof Quadrangle) {
                PaperQuadrangle originalQuadrangle = (PaperQuadrangle) originalFigure;
                if (QuadrangleToQuadrangle(newSideA, newSideB, originalQuadrangle.getSideA(), originalQuadrangle.getSideB()))
                    return new PaperQuadrangle(newSideA, newSideB, ((PaperQuadrangle) originalFigure).getColor());
            }
        } else if (originalFigure instanceof Film) {
            if (originalFigure instanceof Circle) {
                FilmCircle originalCircle = (FilmCircle) originalFigure;
                if (CircleToQuadrangle(newSideA, newSideB, originalCircle.getRadius()))
                    return new FilmQuadrangle(newSideA, newSideB);
            }
            if (originalFigure instanceof Quadrangle) {
                FilmQuadrangle originalQuadrangle = (FilmQuadrangle) originalFigure;
                if (QuadrangleToQuadrangle(newSideA, newSideB, originalQuadrangle.getSideA(), originalQuadrangle.getSideB()))
                    return new FilmQuadrangle(newSideA, newSideB);
            }
        }
        System.out.println("Impossible create a Quadrangle from this " + originalFigure.getClass().getSimpleName());
        return null;
    }

    public static Circle cutCircle(Object originalFigure, double newRadius) throws ParameterException {
        if (originalFigure instanceof Paper) {
            if (originalFigure instanceof Circle) {
                PaperCircle originalCircle = (PaperCircle) originalFigure;
                if (CircleToCircle(newRadius, originalCircle.getRadius()))
                    return new PaperCircle(newRadius,((PaperCircle) originalFigure).getColor());
            }
            if (originalFigure instanceof Quadrangle) {
                PaperQuadrangle originalQuadrangle = (PaperQuadrangle) originalFigure;
                if (QuadrangleToCircle(newRadius, originalQuadrangle.getSideA(), originalQuadrangle.getSideB()))
                    return new PaperCircle(newRadius, ((PaperQuadrangle) originalFigure).getColor());
            }
        } else if (originalFigure instanceof Film) {
            if (originalFigure instanceof Circle) {
                FilmCircle originalCircle = (FilmCircle) originalFigure;
                if (CircleToCircle(newRadius, originalCircle.getRadius()))
                    return new FilmCircle(newRadius);
            } else if (originalFigure instanceof Quadrangle) {
                FilmQuadrangle originalQuadrangle = (FilmQuadrangle) originalFigure;
                if (QuadrangleToCircle(newRadius, originalQuadrangle.getSideA(), originalQuadrangle.getSideB()))
                    return new FilmCircle(newRadius);
            }
        }
        System.out.println("Impossible create a Circle from this " + originalFigure.getClass().getSimpleName());
        return null;
    }


    private static boolean CircleToCircle(double newRadius, double radius) {
        return radius >= newRadius;
    }

    private static boolean QuadrangleToCircle(double newRadius, double sideA, double sideB) {
        return sideA < sideB && newRadius <= sideA || sideA > sideB && newRadius <= sideB;
    }

    private static boolean CircleToQuadrangle(double newSideA, double newSideB, double radius) {
        double diameter = radius * 2;
        double diagonal = Math.sqrt(Math.pow(newSideA, 2) + Math.pow(newSideB, 2));
        if (diameter >= diagonal) {
            System.out.println("Create Quadrangle");
            return true;
        }
        return false;
    }

    private static boolean QuadrangleToQuadrangle(double newSideA, double newSideB, double sideA, double sideB) {
        if (((newSideA >= newSideB) && (sideA >= sideB))
                || ((newSideA <= newSideB) && (sideA <= sideB))
                || (((newSideA <= newSideB) && (sideA >= sideB)
                || ((newSideA >= newSideB) && (sideA <= sideB))))) {
            if (newSideA <= sideA && newSideB <= sideB) {
                System.out.println("Create Quadrangle");
                return true;
            }
        }
        return false;
    }
}
