package figures;

import interfaces.Paper;
import myExcception.ParameterException;
import objectsLogic.Quadrangle;

public class PaperQuadrangle extends Quadrangle implements Paper {


    public PaperQuadrangle(double sideA, double sideB) throws ParameterException {
        setSideA(sideA);
        setSideB(sideB);
    }

    public PaperQuadrangle(double sideA, double sideB, Color color) throws ParameterException {
        setSideA(sideA);
        setSideB(sideB);
        setColor(color);
    }


}
