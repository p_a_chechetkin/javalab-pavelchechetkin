package figures;

import interfaces.Film;
import myExcception.ParameterException;
import objectsLogic.Circle;

public class FilmCircle extends Circle implements Film {

    public FilmCircle() {
    }

    public FilmCircle(double radius) throws ParameterException {
        setRadius(radius);
    }

}
