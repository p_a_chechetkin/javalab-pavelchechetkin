package figures;

import interfaces.Paper;
import myExcception.ParameterException;
import objectsLogic.Circle;

public class PaperCircle extends Circle implements Paper {


    public PaperCircle() {

    }


    public PaperCircle(double radius) throws ParameterException {
        setRadius(radius);
    }

    public PaperCircle(double radius, Color color) throws ParameterException {
        setRadius(radius);
        setColor(color);
    }

}
