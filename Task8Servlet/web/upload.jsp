<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="page" tagdir="/WEB-INF/tags/page"%>

<page:header nameOfPage="Upload on server"/>

<form method="POST" action="upload" enctype="multipart/form-data">
    File:
    <input type="file" name="file" id="file"/> <br/>
    Destination:
    <label>
        <input type="text" value="/temp" name="destination"/>
    </label>
    <input type="submit" value="Upload" name="upl"/>
</form>

<page:endOfsite/>