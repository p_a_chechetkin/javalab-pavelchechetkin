<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="page" tagdir="/WEB-INF/tags/page" %>

<page:header nameOfPage="Download from server"/>

<form method="post" action="download">
    Download File:
    <label>
        <input type="text" value="/temp" name="path"/>
    </label>
    <input type="submit" name="Download"/>
</form>

<page:endOfsite/>
