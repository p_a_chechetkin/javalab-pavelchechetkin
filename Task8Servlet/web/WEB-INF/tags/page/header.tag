<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="nameOfPage" required="true" type="java.lang.String" %>
<html>
<head>
    <title>${nameOfPage}</title>
</head>
<body>
<h1>${nameOfPage}</h1>
<div>
    <a href="/main">Main</a>
    <a href="/doing">Show Or Download</a>
    <a href="/upload">Upload</a>
</div>
<hr/>
