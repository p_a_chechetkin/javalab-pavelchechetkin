import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

/**
 * Created by Admin on 14.12.2017.
 */
@WebFilter(filterName = "Filter", urlPatterns = "/*")
public class Filter implements javax.servlet.Filter {
    public void destroy() {
        System.out.println("LogFilter destroy!");
    }

    public void doFilter(ServletRequest request, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest req = (HttpServletRequest) request;

        HttpSession session = req.getSession(true); // инициируем сессию

        session.setAttribute("username",session.getId()); // добавляем аттрибут

        session.setMaxInactiveInterval(300); // время жизни сессии

        String servletPath = req.getServletPath();
        System.out.println( "  Filter  ");
        System.out.println("#INFO " + new Date() + " - ServletPath :" + servletPath //
                + ", URL = " + req.getRequestURL() + "   " + req.getSession());
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {
        System.out.println("   Filter init!");
    }

}
