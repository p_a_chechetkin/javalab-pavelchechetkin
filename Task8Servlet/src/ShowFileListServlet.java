import javax.print.DocFlavor;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;

/**
 * Created by Admin on 14.12.2017.
 */
@WebServlet(name = "ShowFileListServlet", urlPatterns = "/show")
public class ShowFileListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Show POST");
        response.setContentType("text/html;charset=utf-8");

        String path = request.getParameter("path2");
        System.out.println(path);
        File file = new File(path);

        PrintWriter pw = response.getWriter();

        for (String s : file.list()) {

            pw.println("<H1>" + s + "</H1>");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Show GET");

        request.setAttribute("do","show");
        request.getRequestDispatcher("/show.jsp").forward(request, response);
    }
}
