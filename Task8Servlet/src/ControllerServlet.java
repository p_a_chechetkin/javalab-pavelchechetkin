import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

/**
 * Created by Agentbars on 20.01.2018.
 */

@WebServlet(name = "ControllerServlet", urlPatterns = {"/doing"})
public class ControllerServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Controller GET");
        System.out.println(request.getParameter("do"));
        if (request.getParameter("do").equals("showFiles")){
            request.getRequestDispatcher("/show.jsp").forward(request, response);
        }
        if (request.getParameter("do").equals("downloadFile")){
            request.getRequestDispatcher("/download.jsp").forward(request, response);
        }
    }
}
