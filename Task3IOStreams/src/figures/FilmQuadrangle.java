package figures;

import interfaces.Film;
import objectsLogic.Quadrangle;

public class FilmQuadrangle extends Quadrangle implements Film {
    public FilmQuadrangle() {
    }

    public FilmQuadrangle(double newSideA, double newSideB) {
        setSideA(newSideA);
        setSideB(newSideB);
    }
}
