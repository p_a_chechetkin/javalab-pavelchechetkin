package figures;

import interfaces.Film;
import objectsLogic.Circle;

public class FilmCircle extends Circle implements Film {

    public FilmCircle() {
    }

    public FilmCircle(double radius) {
        setRadius(radius);
    }

}
