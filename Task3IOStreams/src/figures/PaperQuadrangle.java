package figures;

import interfaces.Paper;
import objectsLogic.Quadrangle;

public class PaperQuadrangle extends Quadrangle implements Paper {


    public PaperQuadrangle(double sideA, double sideB) {
        setSideA(sideA);
        setSideB(sideB);
    }

    public PaperQuadrangle(double sideA, double sideB, Color color) {
        setSideA(sideA);
        setSideB(sideB);
        setColor(color);
    }


}
