package figures;

import interfaces.Paper;
import objectsLogic.Circle;

public class PaperCircle extends Circle implements Paper {


    public PaperCircle() {

    }


    public PaperCircle(double radius) {
        setRadius(radius);
    }

    public PaperCircle(double radius, Color color) {
        setRadius(radius);
        setColor(color);
    }

}
