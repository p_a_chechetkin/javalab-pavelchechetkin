package box;


import objectsLogic.Figure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class BoxGirl implements Serializable {

    private static final long serialVersionUID = -596143772957932271L;
    private List<Figure> boxForFigure = new ArrayList<>();

    public void putFigureInBox(Figure figure) {
        if (!boxForFigure.contains(figure)) {
            System.out.println("The figure successful added");
            boxForFigure.add(figure);
        } else {
            System.out.println("The figure is already in the box");
        }
    }

    public void putFigureInBox(Figure... figures) {
        for (Figure figure : figures) {
            if (!boxForFigure.contains(figure)) {
                System.out.println("The figure successful added");
                boxForFigure.add(figure);
            } else {
                System.out.println("The figure is already in the box");
            }
        }
    }

    public Figure lookAtFigure(int number) {
        System.out.println("You look at  " + boxForFigure.get(--number));
        return boxForFigure.get(number);

    }

    public Figure getFigure(int number) {
        Figure figure = boxForFigure.get(--number);
        boxForFigure.remove(number);
        return figure;
    }

    public void replaceThisFigure(int number, Figure newFigure) {
        if (!boxForFigure.contains(newFigure)) {
            boxForFigure.set(--number, newFigure);
            System.out.println("Replaced figure");
        } else {
            System.out.println("The figure " + newFigure.getClass().getTypeName() + " already in box");
        }
    }

    public void viewAll() {
        double area = 0, perimetr = 0;
        for (Figure f : boxForFigure) {
            area += f.area();
            perimetr += f.perimeter();
        }
        System.out.println("Figures in box " + this + ": " + boxForFigure.size() + "\n Summary area = " + area
                + "\n Summary perimeter = " + perimetr + "\n");
    }
}
