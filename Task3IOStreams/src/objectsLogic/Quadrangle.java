package objectsLogic;

public class Quadrangle extends Figure {
    private static transient double sideA, sideB;

    public Quadrangle() {
    }

    public Quadrangle(double sideA, double sideB) {
        Quadrangle.sideA = sideA;
        Quadrangle.sideB = sideB;
    }

    public double getSideA() {
        return sideA;
    }

    protected void setSideA(double sideA) {
        Quadrangle.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    protected void setSideB(double sideB) {
        Quadrangle.sideB = sideB;
    }

    @Override
    public double perimeter() {
        return 2 * sideA + 2 * sideB;
    }

    @Override
    public double area() {
        return sideA * sideB;
    }
}
