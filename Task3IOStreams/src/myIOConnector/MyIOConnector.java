package myIOConnector;

import box.BoxGirl;

import java.io.*;

public class MyIOConnector {

    private static final String FILE_PATH = "src\\serializedObjects\\BoxGirl.out";
    private static final String FILE_PATH_BEGIN = "src\\serializedObjects\\BoxGirl";
    private static final String FILE_PATH_END = ".out";

    public static BoxGirl myDeserialization() throws IOException, ClassNotFoundException {

        ObjectInputStream oin = new ObjectInputStream(new FileInputStream(FILE_PATH));
        return (BoxGirl) oin.readObject();
    }

    public static BoxGirl myDeserializationForObject(Object object) throws IOException, ClassNotFoundException {

        String filePath = FILE_PATH_BEGIN + object.hashCode() + FILE_PATH_END;
        ObjectInputStream oin = new ObjectInputStream(new FileInputStream(filePath));
        BoxGirl boxGirl = null;
        if (object instanceof BoxGirl) {
            boxGirl = (BoxGirl) oin.readObject();
        }
        return boxGirl;
    }

    public static void mySerializationForBox() throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FILE_PATH));
        BoxGirl box = new BoxGirl();
        oos.writeObject(box);
        oos.flush();
        oos.close();
    }

    public static void mySerializationForObject(Object object) throws IOException {
        String filePath = FILE_PATH_BEGIN + object.hashCode() + FILE_PATH_END;
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath));
        oos.writeObject(object);
        oos.flush();
        oos.close();
    }
}
