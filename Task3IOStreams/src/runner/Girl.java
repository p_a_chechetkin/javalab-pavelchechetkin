package runner;

import box.BoxGirl;
import figures.FilmCircle;
import figures.FilmQuadrangle;
import figures.PaperQuadrangle;
import myIOConnector.MyIOConnector;

import java.io.IOException;


public class Girl {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        // Сериализуем пустую коробку
        MyIOConnector.mySerializationForBox();

        // Десериализуем и инициализируем объект коробки
        BoxGirl boxGirl = MyIOConnector.myDeserialization();

        // Создаем, кладем фигуры в коробку, после чего выводим содержимое коробки
        PaperQuadrangle paperQuadrangle = new PaperQuadrangle(3, 4);
        FilmCircle filmCircle = new FilmCircle(2);
        FilmQuadrangle filmQuadrangle = new FilmQuadrangle(3, 4);
        boxGirl.putFigureInBox(paperQuadrangle, filmQuadrangle, filmCircle);

        System.out.println("Box1 with 3 figures");
        boxGirl.viewAll();


        // Сериализуем наполненную коробку в новый файл
        MyIOConnector.mySerializationForObject(boxGirl);

        // Изменяем старую коробку и выводим ее на экран
        boxGirl.getFigure(1);
        System.out.println("Box1 after delete 1 figure");
        boxGirl.viewAll();

        // Десириализуем наполненную коробку
        BoxGirl newBoxGirl = MyIOConnector.myDeserializationForObject(boxGirl);

        // Выводим содержимое новой коробки
        System.out.println("Saved Box2");
        newBoxGirl.viewAll();

    }
}
