import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import org.apache.log4j.Logger;

public class FatherRect {
    static final int SPEED = 10;
    private static final int DELAY = 50;
    Rectangle mainRect;
    Rectangle rectangle = new Rectangle(20, 20);
    int direction = 1;
    Logger log = Logger.getLogger(this.getClass());
    CentrePane centrePane;

    FatherRect(Rectangle mainRect, Pane root, CentrePane centrePane) {
        this.centrePane = centrePane;
        root.getChildren().add(rectangle);
        this.mainRect = mainRect;
    }

    /**
     * изменяет позицию строки
     */
    void move(Point2D nextPoint) {
        // эта часть кода нужна, потому что это JavaFX
        Platform.runLater(() -> {
            // тут сщбственно изменение позиции
            double backX = rectangle.getX(), backY = rectangle.getY();
            rectangle.setLayoutX(nextPoint.getX());
            rectangle.setLayoutY(nextPoint.getY());
            if (rectangle.getX() != backX || rectangle.getY() != backY) {
                log.info(Thread.currentThread().getName() + Thread.currentThread().getId() +
                        " \n - > Now " + nextPoint.getX() + " : " + nextPoint.getY() +
                        " \n - >  <- " + backX + " : " + backY +
                        " \n - > New " + rectangle.getX() + " : " + rectangle.getY());
            }

        });
        try {
            Thread.sleep(DELAY);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * вычисляет следующие координаты строки
     */


    @Override
    public String toString() {
        return " x= " + rectangle.getLayoutX() + " y=" + rectangle.getLayoutY();
    }
}
