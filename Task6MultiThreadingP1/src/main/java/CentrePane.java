import javafx.scene.shape.Rectangle;
import org.apache.log4j.Logger;

import java.util.ArrayList;

class CentrePane {
    Logger log = Logger.getLogger(this.getClass());
    private ArrayList<Rectangle> nodeArrayList = new ArrayList<>();

    ArrayList<Rectangle> getRectosOnCenter() {
        return nodeArrayList;
    }

    synchronized int canGo(Rectangle rectangle, double Board) {
        log.info("  -  -  -  " + Thread.currentThread().getId());

        if (Board >= 200 && Board <= 400) {
            if (this.getRectosOnCenter().isEmpty()) {
                this.getRectosOnCenter().add(rectangle);
            }
            if (!this.getRectosOnCenter().isEmpty() && !this.getRectosOnCenter().contains(rectangle)) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return 0;
            }
        }
        if ((Board < 200 || Board > 400) && this.getRectosOnCenter().contains(rectangle)) {
            this.getRectosOnCenter().remove(rectangle);
            notify();
        }
        return 1;
    }
}
