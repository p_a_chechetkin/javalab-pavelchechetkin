import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.Random;

public class VRect extends FatherRect implements Runnable {

    VRect(Rectangle mainRect, Pane root, CentrePane centre) {
        super(mainRect, root, centre);
        rectangle.setFill(Color.RED);
        Point2D initPoint = getInitPoint(mainRect);
        rectangle.setLayoutX(initPoint.getX());
        rectangle.setLayoutY(initPoint.getY());
    }

    private Point2D getInitPoint(Rectangle mainRect) {
        Random random = new Random();
        int x = 200 + random.nextInt((int) (mainRect.getHeight() - 400 - rectangle.getHeight()));
        int y = 0;
        return new Point2D(x, y);
    }

    @Override
    public void run() {
        log.info("Go " + Thread.currentThread().getName());
        while (true) {
            Point2D nextPoint = getNextPoint();
            move(nextPoint);
        }
    }

    private Point2D getNextPoint() {

        double x = rectangle.getLayoutX();
        double y = rectangle.getLayoutY() + SPEED * direction;
        //---------------------------------------------------------------------------------------------------
        double yBoard = y + 10;
        y = rectangle.getLayoutY() + SPEED * direction * centrePane.canGo(this.rectangle, yBoard);
        //---------------------------------------------------------------------------------------------------

        // вычисление направления движения
        if (y >= mainRect.getWidth() - rectangle.getWidth()) {
            direction = -1;
        }
        if (y <= 0) {
            direction = 1;
        }
        return new Point2D(x, y);
    }

}
