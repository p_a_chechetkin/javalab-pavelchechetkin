package runner;

import data.Detail;

import java.io.IOException;
import java.util.*;

import static reader.MyFileReader.createDetailListFromFile;

public class Runner {

    public static void main(String[] args) throws IOException {

        /* Это решение делает точно так как в примере в задании
        Но, что бы совпадал пример задания и мой результат,
        то часть массива надо было сортировать дополнительно
        и склеивать.
         */
//        FirstSolutionWithTwoLists();

        /* Это решение делает согласно описанной в задании логики
        но без использования обязательных коллекций и методов.
        Это решение работает через рекурсию и без использования новых массивов
         */
//        accordingToLogic();

        newVariant();

        printResultFromExample();


    }

    private static void newVariant() throws IOException {
        List<Detail> detailList = createDetailListFromFile();
        List<Detail> firstPartOfResult = new LinkedList<>();

        recurse(detailList, firstPartOfResult, 0, 0);

        printResult(firstPartOfResult);
    }

    private static void recurse(List<Detail> detailList,
                                List<Detail> firstPartOfResult,
                                int countBegin,
                                int countEnd) {
        Detail detailForOperating = Collections.min(detailList);
        if (detailForOperating.isFirstTimeMin()) {
            firstPartOfResult.add(countBegin++, detailForOperating);
        } else {
            firstPartOfResult.add(firstPartOfResult.size() - countEnd++, detailForOperating);
        }
        detailList.remove(detailForOperating);
        if (detailList.size() > 0) {
            recurse(detailList, firstPartOfResult, countBegin, countEnd);
        }
    }

    private static void printResultFromExample() {
        System.out.println("Result from example" +
                "\n1(1, 5) 3(2, 3) 7(2, 2) 6(4, 5) 8(7, 7) 2(6, 4) 4(6, 3) 5(9, 1)" +
                "\n1(1, 5) 2(6, 4) 3(2, 3) 4(6, 3) 5(9, 1) 6(4, 5) 7(2, 2) 8(7, 7)");
    }

    private static void accordingToLogic() throws IOException {
        List<Detail> dataList = createDetailListFromFile();

        printStartList(dataList);


        processingAccordingToLogic(dataList, 0, dataList.size() - 1);

        printResult(dataList);
    }

    private static void FirstSolutionWithTwoLists() throws IOException {
        List<Detail> dataList = createDetailListFromFile();
        Queue<Detail> endPartOfResult = new LinkedList<>();
        List<Detail> firstPartOfResult = new LinkedList<>();

        printStartList(dataList);

        processAccordingToLogic(dataList, endPartOfResult, firstPartOfResult);

        sortDetailList(firstPartOfResult);

        firstPartOfResult.addAll(endPartOfResult);

        printResult(firstPartOfResult);
    }

    private static void printStartList(List<Detail> dataList) {
        System.out.println("Data list");
        for (Detail d : dataList) {
            System.out.print(d.getNumber() + "(" + d.getFirstTime() + ", " + d.getSecondTime() + ") ");
        }
        System.out.println();
    }

    private static void processingAccordingToLogic(List<Detail> dataList, int startPosition, int lastPosition) {
        if (startPosition <= lastPosition) {
            if (dataList.get(startPosition).isFirstTimeMin()) {
                Detail cont = dataList.get(startPosition);
                dataList.set(startPosition, dataList.get(startPosition));
                dataList.set(startPosition, cont);
                processingAccordingToLogic(dataList, ++startPosition, lastPosition);
            } else {
                Detail cont = dataList.get(lastPosition);
                dataList.set(lastPosition, dataList.get(startPosition));
                dataList.set(startPosition, cont);
                processingAccordingToLogic(dataList, startPosition, --lastPosition);
            }
        }
    }

    private static void processAccordingToLogic(List<Detail> detailList,
                                                Queue<Detail> endPartOfResult,
                                                List<Detail> firstPartOfResult) {
        for (Detail d : detailList) {
            if (d.isFirstTimeMin()) {
                firstPartOfResult.add(d);
            } else {
                endPartOfResult.offer(d);
            }
        }
    }

    private static void sortDetailList(List<Detail> firstPartOfResult) {
        firstPartOfResult.sort(Comparator.comparingInt(Detail::getFirstTime)
                .thenComparing(Comparator.comparingInt(Detail::getSecondTime)));
    }


    private static void printResult(List<Detail> newDetailList) {
        System.out.println("____Result__________");
        for (Detail d : newDetailList) {
            System.out.print(d.getNumber() + "(" + d.getFirstTime() + ", " + d.getSecondTime() + ") ");
        }
        System.out.println();
    }

}
