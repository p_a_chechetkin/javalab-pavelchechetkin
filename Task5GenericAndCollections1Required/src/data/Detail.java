package data;

public class Detail implements Comparable<Detail> {
    private int firstTime, secondTime, number;

    public Detail(int number, int firstTime, int secondTime) {
        setNumber(number);
        setFirstTime(firstTime);
        setSecondTime(secondTime);
    }

    public boolean isFirstTimeMin() {
        return firstTime <= secondTime;
    }

    public int getMinTime(){
        return Math.min(getFirstTime(), getSecondTime());
    }

    public int getFirstTime() {
        return firstTime;
    }

    public void setFirstTime(int firstTime) {
        this.firstTime = firstTime;
    }

    public int getSecondTime() {
        return secondTime;
    }

    public void setSecondTime(int secondTime) {
        this.secondTime = secondTime;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }


    @Override
    public int compareTo(Detail detail) {
        return getMinTime() - detail.getMinTime();
    }
}
