package reader;

import data.Detail;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyFileReader {
    private static String readFirstLine(String pathToFile) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(pathToFile)));
        return reader.readLine();
    }

    public static List<String> readFile(String pathToFile) throws IOException {
        return Files.readAllLines(Paths.get(pathToFile), StandardCharsets.UTF_8);
    }

    public static List<Detail> createDetailListFromFile() throws IOException {
        String FILE_PATH = "Task5GenericAndCollections1Required\\src\\Example.txt";
        String dataLine = MyFileReader.readFirstLine(FILE_PATH);
        List<String> dataList = Arrays.asList(dataLine.split("\\D"));
        List<Detail> detailList = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i += 5) {
            int number = Integer.parseInt(dataList.get(i));
            int firstTime = Integer.parseInt(dataList.get(i + 1));
            int secondTime = Integer.parseInt(dataList.get(i + 3));
            detailList.add(new Detail(number, firstTime, secondTime));
        }
        return detailList;
    }
}
