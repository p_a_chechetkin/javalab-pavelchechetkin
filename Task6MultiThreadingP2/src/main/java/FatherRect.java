import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.Random;

public class FatherRect {

    private final int DELAY = 50;
    int speed;
    Rectangle mainRect;
    Rectangle rectangle = new Rectangle(20, 20);
    int direction = 1;
    CentrePane centrePane;

    FatherRect(Rectangle mainRect, Pane root, CentrePane centrePane) {
        this.centrePane = centrePane;
        setColorSpeed();
        root.getChildren().add(rectangle);
        this.mainRect = mainRect;
    }

    private void setColorSpeed() {
        speed = 10 + (new Random()).nextInt(5);
        switch (new Random().nextInt(4)) {
            case 0:
                rectangle.setFill(Color.BLACK);
                break;
            case 1:
                rectangle.setFill(Color.RED);
                break;
            case 2:
                rectangle.setFill(Color.GREEN);
                break;
            case 3:
                rectangle.setFill(Color.BLUE);
                break;
        }
    }


    /**
     * изменяет позицию строки
     */
    void move(Point2D nextPoint) {
        // эта часть кода нужна, потому что это JavaFX
        Platform.runLater(() -> {
            // тут сщбственно изменение позиции
            rectangle.setLayoutX(nextPoint.getX());
            rectangle.setLayoutY(nextPoint.getY());
        });
        // ------------
        try {
            Thread.sleep(DELAY);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return " x= " + rectangle.getLayoutX() + " y=" + rectangle.getLayoutY();
    }
}
