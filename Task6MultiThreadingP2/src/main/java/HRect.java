import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;

public class HRect extends FatherRect implements Runnable {

    HRect(Rectangle mainRect, Pane root, CentrePane centrePane) {
        super(mainRect, root, centrePane);
        Point2D initPoint = getInitPoint(mainRect);
        rectangle.setLayoutX(initPoint.getX());
        rectangle.setLayoutY(initPoint.getY());
    }

    private Point2D getInitPoint(Rectangle mainRect) {
        java.util.Random random = new java.util.Random();
        int x = 0;
        int y = 200 + random.nextInt((int) (mainRect.getHeight() - 400 - rectangle.getWidth()));

        return new Point2D(x, y);
    }

    @Override
    public void run() {
        rectangle.setAccessibleText(Thread.currentThread().getName());
        while (true) {
            Point2D nextPoint = getNextPoint();
            move(nextPoint);
        }
    }

    private Point2D getNextPoint() {
        double x = rectangle.getLayoutX() + speed * direction;
        double y = rectangle.getLayoutY();
        //---------------------------------------------------------------------------------------------------
        double xBoard = x + 10;
        x = rectangle.getLayoutX() + speed * direction * centrePane.canGo(this.rectangle, xBoard);
        //---------------------------------------------------------------------------------------------------


        // вычисление направления движения
        if (x >= mainRect.getWidth() - rectangle.getWidth()) {
            direction = -1;
        }
        if (x <= 0) {
            direction = 1;
        }

        return new Point2D(x, y);
    }

}
