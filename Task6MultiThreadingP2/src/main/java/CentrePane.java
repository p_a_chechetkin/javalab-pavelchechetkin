import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

class CentrePane {
    private ArrayList<Rectangle> nodeArrayList = new ArrayList<>();

    ArrayList<Rectangle> getRectosOnCenter() {
        return nodeArrayList;
    }

    synchronized int canGo(Rectangle rectangle, double Board) {
        if (Board >= 200 && Board <= 400) {
            if (this.getRectosOnCenter().isEmpty()) {
                this.getRectosOnCenter().add(rectangle);
            }
            if (!this.getRectosOnCenter().isEmpty())
                if (this.getRectosOnCenter().get(0).getFill().equals(rectangle.getFill())
                        && !this.getRectosOnCenter().contains(rectangle)) {
                    this.getRectosOnCenter().add(rectangle);
                }
            if (!this.getRectosOnCenter().isEmpty()
                    && !this.getRectosOnCenter().get(0).getFill().equals(rectangle.getFill())) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return 0;
            }
        }
        if ((Board < 200 || Board > 400) && this.getRectosOnCenter().contains(rectangle)) {
            notifyAll();
            this.getRectosOnCenter().remove(rectangle);
        }
        return 1;
    }
}