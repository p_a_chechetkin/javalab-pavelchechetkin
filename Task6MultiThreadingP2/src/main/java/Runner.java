import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;


public class Runner extends Application {
    private static final int WIDTH = 600;
    private static final int HEIGHT = 600;
    private Rectangle mainRect = new Rectangle(WIDTH, HEIGHT);
    private CentrePane centre = new CentrePane();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Running Lables");
        // завершить работу JVM при закрытии окна
        primaryStage.setOnCloseRequest(event -> {
            javafx.application.Platform.exit();
            System.exit(0);
        });

        Pane root = drawLines();


        // add button to add horizontal Lable
        Button btnHorizontal = new Button();
        btnHorizontal.setText("add horizontal Label");
        btnHorizontal.setMinWidth(200);
        btnHorizontal.setOnAction(event -> new Thread(new HRect(mainRect, root, centre)).start());
        root.getChildren().add(btnHorizontal);

        Button btnVertical = new Button();
        btnVertical.setText("add vertical Label");
        btnVertical.setMinWidth(200);
        btnVertical.setLayoutX(mainRect.getWidth() - 200);
        btnVertical.setOnAction(event -> new Thread(new VRect(mainRect, root, centre)).start());
        root.getChildren().add(btnVertical);

        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
    }

    private Pane drawLines() {
        Pane root = new Pane();
        Line line1 = new Line(200, 0, 200, 600);
        Line line2 = new Line(400, 0, 400, 600);
        Line line3 = new Line(0, 200, 600, 200);
        Line line4 = new Line(0, 400, 600, 400);
        root.getChildren().addAll(line1, line2, line3, line4);
        return root;
    }
}
