package objectsLogic;

public class Quadrangle extends Figure {
    private double sideA, sideB;

    public Quadrangle() {
    }

    public Quadrangle(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public double getSideA() {
        return sideA;
    }

    protected void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    protected void setSideB(double sideB) {
        this.sideB = sideB;
    }

    @Override
    public double perimeter() {
        return 2 * sideA + 2 * sideB;
    }

    @Override
    public double area() {
        return sideA * sideB;
    }
}
