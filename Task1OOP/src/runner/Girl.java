package runner;

import box.BoxGirl;
import figures.FilmCircle;
import figures.FilmQuadrangle;
import figures.PaperCircle;
import figures.PaperQuadrangle;
import interactionLogic.Cutter;
import interfaces.Paper;

public class Girl {
    public static void main(String[] args) {
        /*
        создали 4 фигуры
         */
        PaperQuadrangle paperQuadrangle = new PaperQuadrangle(3, 4);
        PaperCircle paperCircle = new PaperCircle(2.5);
        FilmCircle filmCircle = new FilmCircle(2);
        FilmQuadrangle filmQuadrangle = new FilmQuadrangle(3, 4);
        //создали коробку
        BoxGirl boxGirl = new BoxGirl();
        // складываем фигуры в коробку
        boxGirl.putFigureInBox(paperQuadrangle, filmQuadrangle, filmCircle);
        // смотрим на фигуру по номеру
        boxGirl.lookAtFigure(2);
        // вытаскиваем фигуру из коробки по номеру
        System.out.println("Get figure " + boxGirl.getFigure(2));
        boxGirl.lookAtFigure(2);
        // заменяем фигуру по номеру
        boxGirl.replaceThisFigure(2, filmQuadrangle);
        boxGirl.lookAtFigure(2);
        // смотрим сколько фигур их суммарные периметр и площадь
        boxGirl.viewAll();

        // у всех фигур стандартный цвет Белый
        System.out.println("paperCircle = " + paperCircle.getColor());
        // перекрасим фигуру в красный
        paperCircle.setColor(Paper.Color.RED);
        System.out.println("paperCircle = " + paperCircle.getColor());
        // другие фигуры не перекрашиваются
        System.out.println("paperQuadrangle = " + paperQuadrangle.getColor());
        // повторно перекрасить фигуру нельзя
        paperCircle.setColor(Paper.Color.GREEN);
        System.out.println("paperCircle = " + paperCircle.getColor());

        // Выржеем из круга круг
        PaperCircle paperCircleSecond = (PaperCircle) Cutter.cutCircle(paperCircle,2);
        // у новой фигуры цвет сохраняется от старой
        System.out.println("paperCircleSecond.getColor() = " + paperCircleSecond.getColor());

    }
}
