package programComponents.runner;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


public class Runner {

    public static final String START_DIRECTORY = "src\\resource\\ADC";
    public static final String PREFIX = "ADC_";
    public static final String START_FILE_NAME = "PointOfSaleManageSvRQ_";

    public static void main(String[] args) {
        List<File> files = getListFiles(START_DIRECTORY);

        renameFiles(files);

//        backRenameFiles(files);

    }

    private static List<File> getListFiles(String path) {
        if ((new File(path).listFiles()) != null) {
            return Arrays.asList(new File(path).listFiles());
        }
        return null;
    }

    private static void renameFiles(List<File> files) {
        for (File f : files) {
            if (f.isDirectory()) {

                List<File> childrenList = getListFiles(f.getPath());

                renameFiles(childrenList);

                addPrefics(f);

            } else if (f.isFile()) {

                changeAttributes(f);

                changeFileName(f);

            }
        }
    }

    private static void changeFileName(File f) {
        String newName = f.getParent() + File.separator + f.getName().replaceFirst(START_FILE_NAME,
                START_FILE_NAME + PREFIX);
        f.renameTo(new File(newName));
    }

    private static void backRenameFiles(List<File> files) {
        for (File f : files) {
            if (f.isDirectory()) {

                List<File> childrenList = getListFiles(f.getPath());

                backRenameFiles(childrenList);

                removePrefics(f);

            } else if (f.isFile()) {

                backChangeAttributes(f);

                backChangeNameFile(f);

            }
        }
    }

    private static void backChangeNameFile(File f) {
        String newName = f.getParent() + File.separator +
                f.getName().replaceFirst(START_FILE_NAME + PREFIX,
                        START_FILE_NAME);
        f.renameTo(new File(newName));
    }

    private static void backChangeAttributes(File f) {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse(f.getPath());

            String boxOldData;

            Node pointOfSale = document.getElementsByTagName("PointOfSale").item(0);
            NamedNodeMap attributes = pointOfSale.getAttributes();

            Node parentPointOfSale = attributes.getNamedItem("ParentPointOfSale");
            boxOldData = parentPointOfSale.getTextContent().replaceFirst(PREFIX, "");
            parentPointOfSale.setTextContent(boxOldData);

            Node pointOfSaleCode = attributes.getNamedItem("PointOfSaleCode");
            boxOldData = pointOfSaleCode.getTextContent().replaceFirst(PREFIX, "");
            pointOfSaleCode.setTextContent(boxOldData);


            Element pointOfSaleDescription = (Element) document.getElementsByTagName("PointOfSaleDescription").item(0);
            Attr attrDescription = pointOfSaleDescription.getAttributeNode("Description");
            boxOldData = attrDescription.getValue().replaceFirst(PREFIX, "");
            attrDescription.setValue(boxOldData);

            Transformer transformer = TransformerFactory.newInstance()
                    .newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(f.getPath()));
            transformer.transform(source, result);
        } catch (IOException
                | TransformerException
                | SAXException
                | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    private static void changeAttributes(File f) {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse(f.getPath());

            Node pointOfSale = document.getElementsByTagName("PointOfSale").item(0);
            NamedNodeMap attributes = pointOfSale.getAttributes();

            Node parentPointOfSale = attributes.getNamedItem("ParentPointOfSale");
            parentPointOfSale.setTextContent(PREFIX + parentPointOfSale.getNodeValue());

            Node pointOfSaleCode = attributes.getNamedItem("PointOfSaleCode");
            pointOfSaleCode.setTextContent(PREFIX + pointOfSaleCode.getNodeValue());

            Element pointOfSaleDescription = (Element) document.getElementsByTagName("PointOfSaleDescription").item(0);
            Attr attrDescription = pointOfSaleDescription.getAttributeNode("Description");
            attrDescription.setTextContent(PREFIX + attrDescription.getTextContent());

            Transformer transformer = TransformerFactory.newInstance()
                    .newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(f.getPath()));
            transformer.transform(source, result);
        } catch (IOException
                | TransformerException
                | SAXException
                | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }


    private static void addPrefics(File f) {
        String newName = f.getParent() + File.separator + PREFIX + f.getName();
        if (!f.renameTo(new File(newName))) {
            System.out.println("File" + f.getPath() + " NOT renamed");
        } else {
            System.out.println("File " + f.getPath() + " renamed");
        }
    }

    private static void removePrefics(File f) {
        if (!f.renameTo(new File(f.getParent() + File.separator + f.getName().replaceFirst(PREFIX, "")))) {
            System.out.println(" \t File " + f.getPath() + " NOT renamed!!!");
        } else {
            System.out.println("File " + f.getPath() + " renamed");
        }
    }
}
