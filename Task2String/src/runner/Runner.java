package runner;

import objects.Text;
import reader.MyFileReader;

import java.io.IOException;


public class Runner {

    private static final String FILE_NAME1 = "src\\TextFor2_1.txt";
    private static final String FILE_NAME2 = "src\\TextFor2_2.txt";
    private static final String FILE_NAME3 = "src\\TextFor2_3.txt";
    private static final String FILE_NAME4 = "src\\TextFor2_4.txt";

    public static void main(String[] args) throws IOException {

        mainTask1();

        mainTask2();

        mainTask3();

        mainTask4();
    }

    private static void mainTask1() throws IOException {
        Text text = getTextAndPrint(FILE_NAME1);
        text.deleteCharactersInQuestions();
        printResultText(text);
    }

    private static void mainTask2() throws IOException {
        Text text = getTextAndPrint(FILE_NAME2);
        System.out.println(text.findUniqueWordsFromFirstSentence());
    }

    private static void mainTask3() throws IOException {
        Text text = getTextAndPrint(FILE_NAME3);
        text.swapFirstAndLastWordInMotive();
        printResultText(text);
    }

    private static void mainTask4() throws IOException {
        Text text = getTextAndPrint(FILE_NAME4);
        char firstChar = 'c', secondChar = 'o';
        text.excludeSubstringFromNarrative(firstChar, secondChar);
        printResultText(text);
    }

    private static Text getTextAndPrint(String fileName) throws IOException {
        // Обрабатываем текст
        Text text = new Text(fileName);
        // Выводим текст из файла
        for (String s : MyFileReader.readFile(fileName)) {
            System.out.println(s);
        }
        return text;
    }

    private static void printResultText(Text text) {
        System.out.println(text.getTextAsString());
    }

}
