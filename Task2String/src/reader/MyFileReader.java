package reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class MyFileReader {
    public static List<String> readFile(String pathToFile) throws IOException {
        return Files.readAllLines(Paths.get(pathToFile), StandardCharsets.UTF_8);
    }
}
