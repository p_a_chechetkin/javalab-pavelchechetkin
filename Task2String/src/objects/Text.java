package objects;

import reader.MyFileReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {

    private List<Sentence> sentenceList = new ArrayList<>();

    public Text(String pathToFile) throws IOException {

        Pattern p = Pattern.compile("\"?((Mr\\. )|(Mrs\\. )?([[\\w—',]+\\s]))+([.]{3}|\\p{Punct})\\s?\"?");

        for (String s : MyFileReader.readFile(pathToFile)) {
            Matcher m = p.matcher(s);
            while (m.find()) {
                sentenceList.add(new Sentence(s.substring(m.start(), m.end())));
            }
            sentenceList.add(new Sentence("\n"));
        }
    }

    public void deleteCharactersInQuestions() {
        for (Sentence s : sentenceList) {
            if (s.type.equals(Sentence.Type.QUESTION)) {
                s.deleteCharactersAsFirstCharacterInWords();
            }
        }
    }

    public String getTextAsString() {
        StringBuilder allText = new StringBuilder();
        for (Sentence s : sentenceList) {
            allText.append(s.getSentence());
        }
        return allText.toString();
    }

    public List<String> findUniqueWordsFromFirstSentence() {
        List<String> listOfIndividualWords = new ArrayList<>();
        List<String> listWordsFromFirstSentence = sentenceList.get(0).getListWords();
        for (String searchWord : listWordsFromFirstSentence) {
            int count = 0;
            for (Sentence sentence : sentenceList) {
                if (!sentence.getSentence().contains(searchWord)) {
                    ++count;
                }
            }
            if (count == sentenceList.size() - 1 && !listOfIndividualWords.contains(searchWord)) {
                listOfIndividualWords.add(searchWord);
            }
        }
        return listOfIndividualWords;
    }

    public void swapFirstAndLastWordInMotive() {
        for (Sentence s : sentenceList) {
            if (s.type.equals(Sentence.Type.MOTIVE)) {
                s.swapWords(0, s.getListWords().size() - 1);
            }
        }
    }

    public void excludeSubstringFromNarrative(char firstChar, char secondChar) {
        for (Sentence s : sentenceList) {
            if (s.type.equals(Sentence.Type.NARRATIVE)) {
                s.excludeSubstring(firstChar, secondChar);
            }
        }
    }
}
