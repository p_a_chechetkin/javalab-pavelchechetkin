package objects;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


class WordAndPunctuation {
    private String word = "";
    private String punct = "";
    private int size;

    WordAndPunctuation(String s) {
        String regexForPunctuation = "^[!?.,—\"\n]\\s?$";
        Pattern p = Pattern.compile(regexForPunctuation);
        Matcher m = p.matcher(s);

        while (m.find()) {
            punct = s.substring(m.start(), m.end());
        }
        if (punct.isEmpty()) {
            setWord(s.trim());
            size = s.length();
        } else {
            size = 1;
        }
    }

    String getWord() {
        return word;
    }

    String getWordAndPunct() {
        if (punct.isEmpty()) {
            return word;
        } else {
            return punct;
        }
    }

    void setWord(String word) {
        this.word = word;
    }

    void deleteCharsAsFirst() {
        if (size > 1) {
            word = word.replaceAll(word.substring(0, 1), "");
        }
    }
}
