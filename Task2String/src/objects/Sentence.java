package objects;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {
    Type type = Type.NARRATIVE;
    private List<WordAndPunctuation> wordsAndPunctuationList = new ArrayList<>();

    Sentence(String s) {
        setSentence(s);
    }

    String getSentence() {
        StringBuilder allSentence = new StringBuilder();
        for (int i = 0; i < wordsAndPunctuationList.size(); i++) {
            WordAndPunctuation s = wordsAndPunctuationList.get(i);
            if ((i < wordsAndPunctuationList.size() - 1 && wordsAndPunctuationList.get(i + 1).getWord().isEmpty())
                    || wordsAndPunctuationList.get(i).getWord().isEmpty()) {
                allSentence.append(s.getWordAndPunct());
            } else {
                allSentence.append(s.getWordAndPunct()).append(" ");
            }
        }
        return allSentence.toString();
    }

    private void setSentence(String sentence) {
        wordsAndPunctuationList.clear();
        typeDefinition(sentence);
        String regexForWordOrPunctuation = "([.]{3}|Mr. [A-z]+|Mrs. [A-z]+|[\\p{Punct}—]|[A-z']+|\n)\\s?";
        Pattern p = Pattern.compile(regexForWordOrPunctuation);
        Matcher m = p.matcher(sentence);
        while (m.find()) {
            wordsAndPunctuationList.add(new WordAndPunctuation(sentence.substring(m.start(), m.end())));
        }
    }

    public boolean contains(String searchWord) {
        return getSentence().contains(searchWord);
    }

    private void typeDefinition(String sentence) {
        if (sentence.contains("?")) {
            type = Type.QUESTION;
        } else {
            if (sentence.contains("!")) {
                type = Type.MOTIVE;
            } else {
                if (sentence.contains("\"")) {
                    type = Type.QUOTE;
                }
            }
        }
    }

    void swapWords(int indexFirstWord, int indexSecondWord) {
        List<String> listWords = getListWords();
        String cont = listWords.get(indexFirstWord);
        listWords.set(indexFirstWord, listWords.get(indexSecondWord));
        listWords.set(indexSecondWord, cont);
        setListWords(listWords);
    }

    private void excludeSubstring(String substring) {
        setSentence(getSentence().replaceAll(substring, ""));
    }

    void excludeSubstring(char firstChar, char secondChar) {
        int indexFirstChar = getSentence().indexOf(firstChar);
        int indexSecondChar = getSentence().lastIndexOf(secondChar);
        if (0 < indexFirstChar && indexFirstChar < indexSecondChar) {
            String s = getSentence().substring(indexFirstChar, indexSecondChar + 1);
            excludeSubstring(s);
        }
    }

    void deleteCharactersAsFirstCharacterInWords() {
        for (WordAndPunctuation w : wordsAndPunctuationList) {
            w.deleteCharsAsFirst();
        }
    }

    List<String> getListWords() {
        List<String> words = new ArrayList<>();
        for (WordAndPunctuation w : wordsAndPunctuationList) {
            if (!w.getWord().isEmpty()) words.add(w.getWord());
        }
        return words;
    }

    private void setListWords(List<String> words) {
        int i = 0;
        for (WordAndPunctuation wOrPunct : wordsAndPunctuationList) {
            if (!wOrPunct.getWord().isEmpty()) {
                wOrPunct.setWord(words.get(i++));
            }

        }
    }

    enum Type {MOTIVE, QUESTION, NARRATIVE, QUOTE}


}
