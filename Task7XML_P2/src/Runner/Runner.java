package runner;

import clases.CTNote;
import clases.ListTODO;
import clases.Notes;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class Runner {

    public static final String FILE_FOR_VALIDATOIN = "src\\xmlFiles\\MyGeneratedFile.xml";
    public static final String PATH_TO_XSD = "src\\xmlFiles\\ShemeListTODO.xsd";

    public static void main(String[] args) throws SAXException {

        ListTODO listTODO = new ListTODO();
        Notes notes = new Notes();
        CTNote ctNote;

        for (int i = 0; i < 5; i++) {
            ctNote = new CTNote();
            ctNote.setNote(String.valueOf((new Random()).nextInt()));
            ctNote.setIsDone((new Random()).nextBoolean());
            notes.getENote().add(ctNote);
            listTODO.setToday(notes);
            listTODO.setTomorrow(notes);
            listTODO.setSomeday(notes);
        }

        try {

            File file = new File("src\\xmlFiles\\MyGeneratedFile.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(ListTODO.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(listTODO, file);
            jaxbMarshaller.marshal(listTODO, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }


        SchemaFactory factory =
                SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

        // 2. Компиляция схемы
        // Схема загружается в объект типа java.io.File, но вы также можете использовать
        // классы java.net.URL и javax.xml.transform.Source
        File schemaLocation = new File(PATH_TO_XSD);
        Schema schema = factory.newSchema(schemaLocation);

        // 3. Создание валидатора для схемы
        Validator validator = schema.newValidator();

        // 4. Разбор проверяемого документа
        Source source = new StreamSource(FILE_FOR_VALIDATOIN);

        // 5. Валидация документа
        try {
            validator.validate(source);
            System.out.println("File" + " is valid.");
        } catch (SAXException ex) {
            System.out.println("File" + " is not valid because ");
            System.out.println(ex.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
